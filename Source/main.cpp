//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Array.h"
#include "Number.h"

//template <typename Type>
//Type max (Type a, Type b);
//
//template <typename Type>
//Type min (Type a, Type b);
//
//template <typename Type>
//Type add (Type a, Type b);
//
//template <typename Type>
//Type print (Type genericArray[], int arraySize);
//
//template <typename Type>
//double getAverage (Type genericArray[], int arraySize);

bool testArray();

int main (int argc, const char* argv[])
{

    if (testArray() == true)
    {
        std::cout << "Array tests passed\n";
    }
    else{

        std::cout << "Array tests failed\n";
    }


    return 0;
}




bool testArray()
{
    Array<float> dynamicTest;
    float staticTest[] = {0.1,0.2,0.3,0.4,0.5,0.6,0.7};
    int staticSize = sizeof(staticTest) / sizeof(staticTest[0]);
    
    if (dynamicTest.size() != 0){    // check that dynamic size is 0 at start
        std::cout << "false size at start" << std::endl;
        return false;
    }
    
    for (int i = 0; i < staticSize; i++)        // loop to add static test to dynamic test
    {
        dynamicTest.add(staticTest[i]);         // add value from static test into dynamic test using loop
        
        if(dynamicTest.size() != i+1){              // test dynamic size function
            std::cout << "false size at index: " << i << std::endl;
            return false;
        }
        if(dynamicTest.get(i) != staticTest[i]){    // test dynamic get function
            std::cout << "value not added at index: " << i << std::endl;
            return false;
        }
    }
    
    
    
    
    
    
    return true;            // passes all tests
}























/** Type Templates **/


//template <typename Type>        // a function that returns the maximum of its arguments
//Type max (Type a, Type b)
//{
//    if (a < b)
//        return b;
//    else
//        return a;
//}
//
//template <typename Type>        // a function that returns the minimum of its arguments
//Type min (Type a, Type b)
//{
//    if (a > b)
//        return b;
//    else
//        return a;
//}
//
//template <typename Type>        // a function that returns the sum of its arguments
//Type add (Type a, Type b)
//{
//    return a + b;
//}
//
//template <typename Type>        // a function that takes two arguments, an array of a generic type and an int specifying the size of the array. The function should then print out every item of the array to the console
//Type print (Type genericArray[], int arraySize)
//{
//    for (int i = 0; i <= arraySize; i++) {
//        std::cout << genericArray[i];
//    }
//    return 0;
//}
//
//template <typename Type>        // a function that takes two arguments: asn array of a generic type and an int specifying the size of the array. The function then calculates the mean acerage and returns this value as a double
//double getAverage (Type genericArray[], int arraySize)
//{
//    double temp = 0;
//    for (int i = 0; i < arraySize; i++) {      //add array items to temp variable
//        temp += genericArray[i];
//    }
//    temp = 1.0*temp/arraySize;
//    return temp;                  //divide for average
//}
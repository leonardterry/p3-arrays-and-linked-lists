//
//  Array.cpp
//  CommandLineTool
//
//  Created by Leonard Terry on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

/** Class that manages a dynamic array of floats */

#include "Array.hpp"

Array::Array()
{
    Array::arraySize = 0;
};

Array::~Array()
{
    delete[] arrayPtr;
};

void Array::add (float itemValue)
{
    float* temp = new float[arraySize + 1];                          //make temporary array 1 bigger than existing
    
    for (int count=0; count<arraySize; count++) {
        temp[count] = arrayPtr[count];                  //copy old array into the new array
    }
    
    temp[arraySize] = itemValue;                        //add new value to put into the array
    
    if (arrayPtr != nullptr)
    {
        delete[] arrayPtr;
    }                                                   //delete old array pointer
    
    arrayPtr = temp;                                    //assign array pointer
    
    arraySize++;                                        //update array size
}


float Array::get (int index)const
{
    if(index >= 0 && index < arraySize)
            return arrayPtr[index];
    else
        return 0;
};

int Array::size()
{
    return arraySize;
};



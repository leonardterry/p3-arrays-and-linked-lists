//
//  Array.hpp
//  CommandLineTool
//
//  Created by Leonard Terry on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

/** Class that manages a dynamic array of floats */

#ifndef Array_hpp
#define Array_hpp

#include <stdio.h>


template <class Type>
class Array
{
public:
    
    Array()
    {
        Array::arraySize = 0;
    };
    
    ~Array()
    {
        delete[] arrayPtr;
    };
    
    void add (Type itemValue)
    {
        Type* temp = new Type[arraySize + 1];               //make temporary array 1 bigger than existing
        
        for (int count=0; count<arraySize; count++) {
            temp[count] = arrayPtr[count];                  //copy old array into the new array
        }
        
        temp[arraySize] = itemValue;                        //add new value into the array
        
        if (arrayPtr != nullptr)
            delete[] arrayPtr;                              //delete old array pointer
        
        arrayPtr = temp;                                    //re-assign array pointer
        
        arraySize++;                                        //update array size
    }
    
    
    Type get (int index)const
    {
        if(index >= 0 && index < arraySize)
            return arrayPtr[index];
        else
            return 0;
    };
    
    int size()
    {
        return arraySize;
    };

    
    
    /** Exercise: Operator Overloading **/
    bool operator==(const Array& otherArray)
    {
        if (size() == otherArray.size())
        {
            if (get() == otherArray.get())
            {
                return true;
            }
        }
        return false;
    }
    
    
private:
    int arraySize;
    Type *arrayPtr = nullptr;
};

#endif /* Array_hpp */